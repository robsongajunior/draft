'use strict';

let app;
let path = require('path');
let express = require('express');
let bodyParser = require('body-parser');
let puppeteer = require('puppeteer');
let chai = require('chai');
let expect = chai.expect;

const HOST = '127.0.0.1';
const PORT = 8080;

app = express();

app.use(express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/tests'));
app.use(express.static(__dirname + '/node_modules'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.listen(PORT, HOST);


app.get('/tests', (req, res) => {
    res.sendFile(
        path.join(__dirname + '/html/tests.html')
    );
});


describe('EXTRACT TTFB DATA', () => {
    it('If NOT resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/noparam', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If is EMPTY string resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/emptyparam', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If NULL resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/null', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If UNDEFINED resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/undefined', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If EMPTY OBJECT resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/emptyobject', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If FUNCTION resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/function', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });
});


///////////////////////////////
// PUPPETEER BROWSER OPENING//
/////////////////////////////


(async() => {
    let browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ]
        //headless: false
    });
    let page = await browser.newPage();

    await page.goto('http://localhost:8080/tests');
})();