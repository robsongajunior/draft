describe('EXTRACT TTFB DATA', function() {
    it('If NOT resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
            
            data.expect = window.getTTFB();
            data.value = '';
            data.path = 'noparam';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If is EMPTY string resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};

            data.expect = window.getTTFB('');
            data.value = '';
            data.path = 'emptyparam';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If NULL resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};

            data.expect = window.getTTFB(null);
            data.value = '';
            data.path = 'null';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If UNDEFINED resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
          
            data.expect = window.getTTFB(undefined);
            data.value = '';
            data.path = 'undefined';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If EMPTY OBJECT resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
            
            data.expect = window.getTTFB({});
            data.value = '';
            data.path = 'emptyobject';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If FUNCTION resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
            
            data.expect = window.getTTFB(function(){});
            data.value = '';
            data.path = 'function';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });
});
