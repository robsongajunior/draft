'use strict';

let path = require('path');
let express = require('express');

const HOST = '127.0.0.1';
const PORT = 8080;
const app = express();

app.listen(PORT, HOST);

app.use(express.static(__dirname + '/tests'));
app.use(express.static(__dirname + '/node_modules'));

// CLIENT COLLECTION SAMPLE
app.get('/', (req, res) => {
 	res.sendFile(
		path.join(__dirname + '/html/tests.html')
	);
});


// CLIENT COLLECTION SAMPLE
app.get('/tests', (req, res) => {
 	res.sendFile(
		path.join(__dirname + '/html/tests.html')
	);
});

console.log(`Running on ${HOST}:${PORT}`);

