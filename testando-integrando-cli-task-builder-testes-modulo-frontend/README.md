#  Testando e integrando no CLI task runner um módulo front-end sem interface.

<p align="center"> <img src ="./img/integration.png"/> </p>

Para um bom entendimento deste artigo, recomenda-se estar familiarizado com:

- distinções entre
  - module
  - app
  - ui

Mas caso essa nomenclatura não for familiar, não se preocupe.

- protocolo HTTP
  - POST
  - GET

Neste artigo, tenho como objetivo abordar uma maneira de como podemos integrar nossos testes unitários/funcionais de um módulo front-end que não possui interface, junto a  uma build task.


> **NOTE:** Este artigo não tem como objetivo, TDD, BDD ou qualquer outra abordágem metódica.
> O objetivo é ter um  entendimento arqueturial, podendo então,ser usada qualquer metodologia a partir de então;



## MÓDULO FRONT-END SEM INTERFACE 
sso mesmo, porém não se assuste. Farei uma rasa revisão sobre os conceitos:

- module
- app
- ui


### MODULE
De uma tradução literal, módulo.
Somos acostumados a chamar de módulo tudo que é de responsabilidade unica ou aquele pedaço de código que possa ser importado para realizar determinado processamento.

Errado?
Não, mas devemos ter muito cuidado quando falamos de módulos e front-end.

Módulo também é aquilo que faz algo, porém:

- não toca na interface
- não estiliza
- não faz append
- não causa [repaint/reflow](https://tableless.com.br/repaint-reflow-layout-thrashing-performance-alem-do-carregamento/).

Um exemplo disso é um uso das seguintes  browser API's:
- [Navigation Timing API](https://developer.mozilla.org/pt-BR/docs/Web/API/Navigation_timing_API)
  - A API Navigation Timing fornece dados que podem ser usados para medir a performance de um website. Diferente de outros mecanismos baseados em Javascript que já foram usados para o mesmo propósito, esta API pode fornecer dados sobre a latência do começo ao fim que podem ser mais precisas e relevantes.
- [Resource Timing API](https://developer.mozilla.org/pt-BR/docs/Web/API/Navigation_timing_API)
  - As interfaces de tempo de recursos permitem recuperar e analisar dados de tempo de rede detalhados sobre o carregamento de recursos de um aplicativo.
- [Network Information API](https://developer.mozilla.org/en-US/docs/Web/API/Network_Information_API)
  - A API de Informações de Rede fornece informação sobre a conexão do sistema, assim como a banda atual do dispositivo do usuário ou qualquer conexão que seja medida. Essa pode também ser usada para selecionar conteúdo de alta ou baixa definição baseado na conexão do usuário. 

``` javascript
// parâmetro resource é o objeto que foi pego
// através da API de Resource Timing

window.getTTFB = function(resource) {
    var resourceKeys = [];

    if(!resource) {
        return '';
    }

    resourceKeys = Object.keys(resource);
    if(
        resourceKeys.indexOf('responseStart') === -1 ||
        resourceKeys.indexOf('requestStart') === -1
    ) {
        return '';
    }

    return resource.responseStart - resource.requestStart;
};
```

Este exemplo é uma função que trata apenas informações de um recurso do browser, ou seja, não toca na interface para processar e chegar em algum resultado, podendo ser um método de um módulo.

### APP

Um app pode tanto processar informação mas é conhecido também por além de processar ele poder manipular a interface. Podemos dizer que um slider/carroussel poderia ser um app. Além de processar índices atuais, ele cuidará da passagem do slider, com isso irá manipular a interface.


### UI
 
Como a abreviação já sugere, User Interface (interface do usuário), essa parte será composta por:

- um ou n módulos
- um ou n apps

É comum chamarmos de'web pages, onde tem'vários componentes, vários comportamentos e diferentes resultados visuais.




## INICIO DE TUDO
Há poucos meses, entrei para o time de Sudoers da [Azion](https://www.azion.com/), desde então, tenho trabalhado em um módulo escrito em JavaScript que irá rodar nos dispositivos Client-Side.
Esse módulo é ocorre o build através do NodeJS junto com a ajuda de algumas ferramentas como: Npm script, Grunt, Express, Puppeteer, JSHint.




## TESTANDO
Nós escolhemos usar para teste o framework [Mocha](https://mochajs.org) e [Chai](http://www.chaijs.com) para realizar os asserts.
Então, ok imagina o seguinte.

Temos a seguinte função que será rodada no client-side.

``` javascript
// parâmetro resource é o objeto que foi pego
// através da API de Resource Timing


/*
SAMPLE of resource PARAM 

{  
   "name":"https://410001a.ha.azioncdn.net/probe/100k.bin",
   "entryType":"resource",
   "startTime":1542.0999999987544,
   "duration":774.0999999987253,
   "initiatorType":"",
   "nextHopProtocol":"h2",
   "workerStart":0,
   "redirectStart":0,
   "redirectEnd":0,
   "fetchStart":1542.0999999987544,
   "domainLookupStart":1542.0999999987544,
   "domainLookupEnd":1542.0999999987544,
   "connectStart":1542.0999999987544,
   "connectEnd":1542.0999999987544,
   "secureConnectionStart":0,
   "requestStart":1543.099999998958,
   "responseStart":2292.3999999984517,
   "responseEnd":2316.1999999974796,
   "transferSize":102899,
   "encodedBodySize":102400,
   "decodedBodySize":102400,
   "serverTiming":[]
}
 */

window.getTTFB = function(resource) {
    var resourceKeys = [];

    if(!resource) {
        return '';
    }

    resourceKeys = Object.keys(resource);
    if(
        resourceKeys.indexOf('responseStart') === -1 ||
        resourceKeys.indexOf('requestStart') === -1
    ) {
        return '';
    }

    return resource.responseStart - resource.requestStart;
};

var rsc = window.performance.getEntriesByType('resource');
console.log(window.getTTFB(rsc[x]));
```

> Note, não temos uma interface onde iremos jogar o resultado, esse resultado será usado para processar algo e após isso será salvo em algum database.


```javascript
// testing
describe('EXTRACT TIME TO FIRST BYTE DATA', function() {
  it('If FUNCTION resource param, should be return a empty string', function() {
    return new Promise(function(resolve, reject) {
     let data = {};
     data.expect = getTTFB({});
     data.value = '';
      
     chai.expect(data.expect).to.equal('');
      
     resolve();
  });
});
```

Nesse teste estamos executando uma função, esperamos uma string vazia como retorno, isso, caso tenha sido passado como parâmetro um objeto JavaScript vazio;


## COMO RODO O TESTE?

<p align="center"> <img src ="./img/test-browser.png"/> </p>

Deverá ser aberto em algum browser, seja ele físico ou headless.
Até aí tudo bem, o teste roda e o resultado é mostrado através da interface web.
Porém, por conta disso, começamos a encontrar alguns obstáculos para integração com o task builder rodado em tempo de Continuous Integration e Continuous Delivery, 




## O PROBLEMA


### PROBLEMA 01
Se prestou bem atenção, notou que obrigatóriamente temos que abrir uma url específica em um browser, seja físico ou headless para que pudéssemos rodar e de alguma forma ver o resultado do nosso teste


### PROBLEMA 02
Usando uma versão headless. Ele irá rodar quando der o comando e não mostrará nada, então de alguma forma ainda estamos dependentes de uma interface.


### PROBLEMA 03
Um módulo não escreve nada na interface, diminuindo a utilidade de screenshots

### PROBLEMA 04
Não contendo  mudanças na interface, não podemos utilizar métodos como querySelector ou qualquer outro do mesmo genero.


#### POSSÍVEL SOLUÇÃO
Poderiamos levantar um selenium/pupetteer e abrir a url de teste, com isso pegar o elemento onde mostra o total dos testes e verificar se no elemento que tem os erros ainda é igual a zero.


### MAIS PROBLEMAS

#### PROBLEMA 05
Com essa solução, até funcionaria, porém, é uma kid solution. Após a execução da página se ficou com zero errors pegando o elemento que indica sucesso ou erro.
Porém, temos algumas desvantagens:

-  teriamos sempre que abrir manualmente a página para saber onde é o real teste que falhou, dando aquele trabalho desnecessário e nada produtivo sendo detecção manual;
- inviabilizaria uma monitoração simples e precisa;
- não teriamos como salvar logs diferentes de true/false;
- para resolver todos problemas a cima teriamos que criar um webscrap da página de resultados de testes, INVIÁVEL;




## STOP PROBLEMS, START FIXING

### LINHA DE RACIOCÍNIO
Após eu ter os testes escritos no front-end, o próximo passo seria conseguir pegar o resultado desses testes de alguma maneira. Para início de tudo, eu já tinha uma certeza, de qualquer forma irá ter que ser aberto um browser, então como mandar as respostas para um CLI?



## ARQUITETURA

<p align="center"> <img src ="./img/test-fluxogram.png"/> </p>


### DESCREVENDO ARQUITETURA

Tudo começa com a execução do arquivo **test-ci.js**.

|     PASSO    |   DESCRIÇÃO   |
|---------------------|-------------------------|
|          01        |  comando do npm script |
|          02        |  comando dado no passo antes levanta um httpserver local |
|          03        |  abre a url usando puppeteer (chrome headless)  |
|          04        |  GET na url da página de testes |
|          05        |  httpserver levantado recebe o pedido da pagina de teste |
|          06        |  httpserver processa a página HTML que contém os testes |
|          07        |  httpserver devolve a página HTML que contém os testes |
|          08        |  browser roda os testes |
|          09        |  cada teste uma url para POST result |
|          10        |  POST do resultado para o httpserver levantado |
|          11        |  httpserver recebe e testa o resultado |
|          12        |  httpserver mostra resultado do teste no CLI |



### DESCREVENDO NA PRÁTICA

Imagine que estamos no nível raiz do projeto, dentro temos a estrutura a seguir..

- root
  - html/
    - tests.html
  - tests/
    - tests.js
  - test-ci.js


``` html
<!-- ARQUIVO >> html/tests.html -->

<html>
<head>
    <meta charset="utf-8">

  <title>Mocha Tests</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

    <link href="https://cdn.rawgit.com/mochajs/mocha/2.2.5/mocha.css" rel="stylesheet" />
</head>
<body>
  <div id="mocha"></div>

  <script src="https://cdn.rawgit.com/mochajs/mocha/2.2.5/mocha.js"></script>
  <script src="chai/chai.js"></script>

  <script type="text/javascript">
    mocha.setup('bdd');
  </script>

  <script type="text/javascript">
    window.postResult = /postresult=false/gi.test(window.location.search);
    window.sendTestResult = function(data) {
      var opts = {};      
      var info = {};
      info.expect = data.expect;
      info.value = data.value;

      if(window.postResult) {
          return Promise.resolve();
      }

      opts = {
        method: 'POST',
        body: JSON.stringify(info),
        mode: 'cors',
        headers: {
            "Content-Type": "application/json"
        }
      };
      return fetch('//localhost:8080/' + data.path, opts);
    };
    window.getTTFB = function(resource) {
      var resourceKeys = [];

      if(!resource) {
          return '';
      }

      resourceKeys = Object.keys(resource);
      if(
          resourceKeys.indexOf('responseStart') === -1 ||
          resourceKeys.indexOf('requestStart') === -1
      ) {
          return '';
      }

      return resource.responseStart - resource.requestStart;
  };
  </script>
  
  <!-- script tests -->
  <script type="text/javascript" src="tests.js"></script>
  <!-- end script tests -->
  
  <script type="text/javascript">
      mocha.checkLeaks();
      mocha.run();
  </script>
</body>
</html>
```

``` javascript

// ARQUIVO >> tests/tests.js

describe('EXTRACT TTFB DATA', function() {
    it('If NOT resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
            
            data.expect = window.getTTFB();
            data.value = '';
            data.path = 'noparam';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If is EMPTY string resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};

            data.expect = window.getTTFB('');
            data.value = '';
            data.path = 'emptyparam';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If NULL resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};

            data.expect = window.getTTFB(null);
            data.value = '';
            data.path = 'null';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If UNDEFINED resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
          
            data.expect = window.getTTFB(undefined);
            data.value = '';
            data.path = 'undefined';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If EMPTY OBJECT resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
            
            data.expect = window.getTTFB({});
            data.value = '';
            data.path = 'emptyobject';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });

    it('If FUNCTION resource param, should be return a empty string', function() {
        return new Promise(function(resolve, reject) {
            var data = {};
            
            data.expect = window.getTTFB(function(){});
            data.value = '';
            data.path = 'function';

            chai.expect(data.expect).to.equal(data.value);

            window.sendTestResult(data).then(resolve);
        });
    });
});
```
``` javascript
// ARQUIVO >> ./root/test-ci.js 

'use strict';

let app;
let path = require('path');
let express = require('express');
let bodyParser = require('body-parser');
let puppeteer = require('puppeteer');
let chai = require('chai');
let expect = chai.expect;

const HOST = '127.0.0.1';
const PORT = 8080;

app = express();

app.use(express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/tests'));
app.use(express.static(__dirname + '/node_modules'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.listen(PORT, HOST);


app.get('/tests', (req, res) => {
    res.sendFile(
        path.join(__dirname + '/html/tests.html')
    );
});


describe('EXTRACT TTFB DATA', () => {
    it('If NOT resource param, should be return a empty string', () => {
        return new Promise(function(resolve, reject) {
            app.post('/noparam', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If is EMPTY string resource param, should be return a empty string', () => {
    return new Promise(function(resolve, reject) {
            app.post('/emptyparam', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If NULL resource param, should be return a empty string', () => {
    return new Promise(function(resolve, reject) {
            app.post('/null', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If UNDEFINED resource param, should be return a empty string', () => {
    return new Promise(function(resolve, reject) {
            app.post('/undefined', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If EMPTY OBJECT resource param, should be return a empty string', () => {
    return new Promise(function(resolve, reject) {
            app.post('/emptyobject', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });

    it('If FUNCTION resource param, should be return a empty string', () => {
    return new Promise(function(resolve, reject) {
            app.post('/function', (req, res) => {
                expect(req.body.expect).to.equal(req.body.value);

                res.end();
                resolve();
            });
        });
    });
});


///////////////////////////////
// PUPPETEER BROWSER OPENING//
/////////////////////////////


(async() => {
    let browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ]
        //headless: false
    });
    let page = await browser.newPage();

    await page.goto('http://localhost:8080/tests');
})();
```



### DESCREVENDO ARQUIVOS

|     ARQUIVO     |   DESCRIÇÃO   |
|---------------------|-------------------------|
|  html/tests.html |  contém HTML com estrutura do framework de testes, e método global simulando módulo e uma função para postar resultado para o HTTPSERVER|
|  tests/tests.js     |  contém os testes front-end e a cada teste feito é disparado um post para o mesmo localhost que levantou a página com o resultado dos asserts  |
|  test-ci.js             |  levanta HTTPSERVER localhost, levanta via pupetteer url dos testes,  httpserver recebe via POST o resultado dos testes, faz asserts dos resultados |



### RODANDO NA VIDA REAL
Bom, agora vamos rodar no noso nodejs e ver se realmente funciona.

``` bash
clear && npm install && npm run test
```

<p align="center"> <img src ="./img/test-result.png"/> </p>



### RECAPTULANDO
Apenas para relembrar o que usamos e como pensamos para chegar nessa solução.

- tinhhamos um módulo que não modifica a interace
- deveriamos passar os valores para um task build
- os mesmos testes que resultam no browser deveriamos saber em tempo de task build

Então, basicamente replicamos os testes do front-end no task build porém apenas recebendo os valores já testados no front-end.
Isso devido de uma arquitetura que os testes levantam um localhost e no client cada teste feito é postado o resultado para o mesmo localhost que levantou a página de testes.




## VANTAGENS
- conseguimos repassar os resultados no real formato que foram gerados
- conseguimos integrar ao CLI de uma forma simples e eficaz
- integrado no CLI conseguimos integrar ao nosso processo de CI/CD
- podemos criar uma monitoração




## DESVATAGENS
- um pequeno retrabalho de escrever de forma manual os testes dos dois lados
- estrutura bem recente, não muito discutida




## REFERÊNCIAS
- [pupetteer ](https://github.com/GoogleChrome/puppeteer)
- [mocha](https://mochajs.org/)
- [chai](https://www.chaijs.com/)
- [express](https://expressjs.com/pt-br/)
- [exemplo em código ](https://gitlab.com/robsongajunior/draft/tree/master/testando-integrando-cli-task-builder-testes-modulo-frontend/sample)